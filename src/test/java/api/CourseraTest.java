package api;

import com.headlezz.config.SpringMongoConfig;
import com.headlezz.external.CourseraApi;
import com.headlezz.service.SkillService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.Arrays;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringMongoConfig.class)
public class CourseraTest {
    @Autowired
    private CourseraApi courseraApi;

    @Autowired
    private SkillService skillService;

    @Test
    public void testRetrieve() throws IOException {
        courseraApi.getCourses(Arrays.asList("java"));
    }

}
