package api;

import com.headlezz.config.SpringMongoConfig;
import com.headlezz.external.HeadHunterAPI;
import com.headlezz.external.dto.SearchVacanciesDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringMongoConfig.class)
public class HhApiTest {
    @Autowired
    private HeadHunterAPI headHunterAPI;

    public static final String DTO_TEXT = "java+developer";

    @Test
    public void test() throws IOException {
        SearchVacanciesDto dto = new SearchVacanciesDto();
        dto.setArea("5");
        dto.setText(DTO_TEXT);
        List<String> vacanciesBodies = headHunterAPI.getVacanciesBodies(dto);
        System.out.println(vacanciesBodies.size());
        /*for (String string : vacanciesBodies){
            System.out.println(string);
            System.out.println("\n");
        }*/
    }

    @Test
    public void test2() throws IOException {
        SearchVacanciesDto dto = new SearchVacanciesDto();
        dto.setArea("5");
        dto.setText("java");
        System.out.println(headHunterAPI.getVacanciesBodies(dto).size());
    }
}
