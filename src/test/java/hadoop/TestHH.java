package hadoop;

import com.headlezz.config.SpringMongoConfig;
import com.headlezz.service.SkillService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringMongoConfig.class})
public class TestHH {
    @Autowired
    private SkillService skillService;

    @Test
    public void test() throws JobInstanceAlreadyCompleteException, InterruptedException, JobParametersInvalidException, JobExecutionAlreadyRunningException, IOException, JobRestartException, ClassNotFoundException {
        skillService.updateRanks();
    }
}
