package oauth;

import com.headlezz.config.SpringMongoConfig;
import com.headlezz.model.User;
import com.headlezz.model.oauth.FacebookOauthUser;
import com.headlezz.repository.UserRepo;
import com.headlezz.service.FacebookAuthorizationService;
import com.headlezz.ws.auth.FacebookConnection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringMongoConfig.class})
public class FacebookAuthorizationServiceTest {
    @Autowired
    private FacebookAuthorizationService facebookAuthorizationService;
    @Qualifier("userRepo")
    @Autowired
    private UserRepo userRepo;

    @Test
    public void testFindUserByEmail() {
        FacebookOauthUser oauthUser = new FacebookOauthUser();
        oauthUser.setEmail("lopushen@gmail.com");
        facebookAuthorizationService.authorize(oauthUser);

    }

    @Test
    public void shouldInsertUser() {
        User user = new User();
        user.setEmail("lopushen@gmail.com");
        userRepo.save(user);
    }

    @Test
    public void shouldDoFacebookOauthUser() {
        FacebookConnection facebookConnection = new FacebookConnection();
        String oauthUrl = facebookConnection.getFBAuthUrl();
        System.out.println(oauthUrl);
//        String accessToken = facebookConnection.getAccessToken(code);
//
//        FacebookUserFetcher fbGraph = new FacebookUserFetcher(accessToken);
//        String graph = fbGraph.getFBGraph();
//        System.out.println(fbGraph.fetchUser(graph));
    }
}
