package repo;

import com.headlezz.common.enums.SkillType;
import com.headlezz.config.SpringMongoConfig;
import com.headlezz.model.Skill;
import com.headlezz.model.Tag;
import com.headlezz.repository.SkillRepo;
import com.headlezz.repository.SystemDataRepo;
import com.headlezz.service.SkillService;
import com.headlezz.service.TagService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringMongoConfig.class)
public class MainTest {
    @Autowired
    private SkillService skillService;
    @Autowired
    private SkillRepo skillRepo;
    @Autowired
    private TagService tagService;
    @Autowired
    private SystemDataRepo systemDataRepo;

    @Test
    public void test1() throws Exception {
        List<Skill> testTag = skillRepo.findByTagRanksTagName("testTag");
        for (Skill skill : testTag) {
            System.out.println(skill);
        }
    }

    @Test
    public void test2() {
        Tag tag = new Tag();
        tag.setName("front-end");

        tagService.addTag(tag);

    }

    @Test
    public void testRecommendedSkills(){
        for (Skill skill : skillService.getRecommendedUserSkills(new ArrayList<Skill>(){{
            add(Skill.getBuilder().setName("junit")
                    .setDescription("JUnit is a unit testing framework for the Java programming language. JUnit has been important in the development of test-driven development, and is one of a family of unit testing frameworks which is collectively known as xUnit that originated with SUnit.")
                    .setType(null).build());}}, "java")){
            System.out.println(skill.getName() + " --- " + skill.getTagRanks());
        }
    }

    @Test
    public void testGetAllSkills(){
        System.out.println(skillService.getUserSkills(""));
    }


}
