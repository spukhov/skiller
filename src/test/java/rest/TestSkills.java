package rest;

import com.headlezz.model.Link;
import com.headlezz.model.Skill;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;

public class TestSkills {
    private Client client = ClientBuilder.newClient();
    public final String URL_BASE = "http://localhost:8080/skiller/api";
    private String URL_SKILLS = "/skills";
    private String SPRING = "SPRING";


    public Client getClient() {
        return client;
    }

    public void printResponse(Response res) {
        System.out.println("Output from Server .... \n");
        String output = res.readEntity(String.class);
        System.out.println(output);
    }


    @Test
    public void testAddSkill() {
        Skill skill = new Skill();
        skill.setName("php");
        skill.setDescription("PHP is a server-side scripting language designed for web development but also used as a general-purpose programming language.");
        skill.setImageUrl("https://d2imkaiqu95sh.cloudfront.net/assets/img/php_logo.png");
        Response res = getClient().target(URL_BASE + URL_SKILLS).
                request(MediaType.APPLICATION_JSON).post(Entity.entity(skill, MediaType.APPLICATION_JSON));

        printResponse(res);
    }

}
