package com.headlezz.service;

import com.headlezz.external.model.UserResponse;
import com.headlezz.external.util.FacebookUserConverter;
import com.headlezz.model.User;
import com.headlezz.model.oauth.FacebookOauthUser;
import com.headlezz.repository.UserRepo;
import com.headlezz.ws.auth.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FacebookAuthorizationService implements AuthorizationService<FacebookOauthUser> {
    @Autowired
    private UserRepo userRepo;
    private TokenService tokenService = new TokenService();
    private FacebookUserConverter facebookUserConverter = new FacebookUserConverter();

    @Override
    public UserResponse authorize(FacebookOauthUser oauthUser) {
        User user = facebookUserConverter.convertToSkillerUser(oauthUser);

        User userFromDb = userRepo.findByEmail(user.getEmail());
        if (userFromDb == null) {
            userRepo.save(userFromDb);
        }
        String systemToken = tokenService.generateToken(user);
        UserResponse userResponse = new UserResponse();
        userResponse.setEmail(user.getEmail());
        userResponse.setName(user.getName());
        userResponse.setSystemToken(systemToken);
        return userResponse;
    }
}
