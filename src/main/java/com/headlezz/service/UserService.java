package com.headlezz.service;

import com.headlezz.model.User;
import com.headlezz.repository.UserRepo;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserService {
    private final short TOKEN_SIZE = 20;

    @Autowired
    private UserRepo userRepo;

    /*public String authorize(Credentials creds) {
        User existingUser = userRepo.findByEmail(creds.getEmail());
        System.out.println(Encription.checkPassword(existingUser.getPassword(),
                Encription.encode(creds.getPassword())));
        if (Encription.checkPassword(creds.getPassword(), existingUser.getPassword())) {
            return existingUser.getToken();
        }
        return null;
    }

    public void register(Credentials creds) throws DuplicatedEmailException {
        String email = creds.getEmail();
        if (userRepo.findByEmail(email) != null) {
            throw new DuplicatedEmailException("email already exists: " + email);
        }

        User user = userRepo.save(new User(email, Encription.encode(creds.getPassword()), generateToken()));
        System.out.println(user.toString());
        userDataRepo.save(new UserData(user.getToken()));

    }*/

    public long getUserCount() {
        return userRepo.count();
    }

    public String generateToken() {
        return RandomStringUtils.randomAlphanumeric(TOKEN_SIZE);
    }

    public User findByToken(String token) {
        return new User(); // TODO find by token
    }

    public void updateUser(User user) {
        userRepo.save(user);
    }
}
