package com.headlezz.service;

import com.google.common.collect.Lists;
import com.headlezz.external.CourseraApi;
import com.headlezz.external.model.CourseShort;
import com.headlezz.external.model.CoursesShort;
import com.headlezz.model.Link;
import com.headlezz.model.Skill;
import com.headlezz.model.User;
import com.headlezz.repository.SkillRepo;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang3.mutable.MutableFloat;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

@Service
@Log4j
public class SkillService {
    public static final int RECOMMENDED_SKILLS_SIZE = 50;
    @Autowired
    private SkillRepo skillRepo;
    @Autowired
    private CourseraApi courseraApi;
    @Autowired
    private UserService userService;
    @Autowired
    private TagService tagService;


    public Skill saveSkill(Skill skill) throws Exception {
        Skill savedSkill = skillRepo.save(skill);
        if (savedSkill == null) {
            throw new Exception(String.format("Cannot save '%s' to db.", skill));
        }
        return savedSkill;
    }

    public List<Skill> getAllSkills(String s) {
        List<Skill> all = skillRepo.findAll();
        return all;

    }


    private List<Link> getSkillCourses(String name) {
        List<Link> links = new ArrayList<>();
        try {
            CoursesShort coursesShort = courseraApi.getCourses(Arrays.asList(name));
            fillInLinksWithCoursera(links, coursesShort);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return links;
    }

    private void fillInLinksWithCoursera(List<Link> links, CoursesShort coursesShort) {
        for (CourseShort courseShort : coursesShort.getElements()) {
            Link link = new Link(courseShort.getName(), "coursera", courseShort.getLink());
            System.out.println(link);
            links.add(link);
        }
    }

    public void addUserSkill(String token, String skillId) {
        User user = userService.findByToken(token);
        user.getOwnedSkillIds().add(skillId);
        userService.updateUser(user);
    }

    /*@Deprecated // TODO uncomment and test when tags would be returned
    public List<Skill> getRecommendedUserSkills(String token) {
        Map<Skill, MutableFloat> result = new HashMap<>();
        List<String> userTagNames = getUserTagNames(token);

        for (String tagName : userTagNames) {
            // TODO limit skills by tag, no need to fetch ALL skills
            List<Skill> tagSkills = getSkillsByTagName(tagName);
            for (Skill skill : tagSkills) {
                float skillTagRate = skill.getTagRanks().stream()
                        .filter(tagRank -> tagRank.getTagName().equals(tagName))
                        .collect(Collectors.toList()).get(0).getRank();
                MutableFloat index = result.get(skill);
                if (index != null) {
                    index.add(skillTagRate);
                } else {
                    result.put(skill, new MutableFloat(skillTagRate));
                }
            }
        }
        return getTopRecommendedSkillsFromMap(result);
    }*/

    public List<Skill> getRecommendedUserSkills(List<Skill> userSkills, String profession) {
        List<Skill> skillsByTagName = getSkillsByTagName(profession);
        for (Skill skill : skillsByTagName) {
            if (userSkills.contains(skill)) {
                skillsByTagName.remove(skill);
            }
        }
        return skillsByTagName;
    }

    private List<Skill> getSkillsByTagName(String tagName) {
        return skillRepo.findByTagRanksTagName(tagName);
    }

    private Collection<Skill> getTopRecommendedSkillsFromMap(Map<Skill, MutableFloat> skillRates) {
        return skillRates.entrySet().stream()
                .sorted((e1, e2) -> e1.getValue().compareTo(e2.getValue()))
                .limit(RECOMMENDED_SKILLS_SIZE)
                .collect(HashMap<Skill, MutableFloat>::new, (m, v) -> m.put(v.getKey(), v.getValue()), HashMap::putAll).keySet();
    }


    public void updateRanks() throws IOException, JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException, InterruptedException, ClassNotFoundException {
        //       hhAnalyzer.run();
        //TODO need to implement
    }

    public List<String> getUserTagNames(String token) {
        User user = userService.findByToken(token);
        List<String> tagNames = user.getTagNames(); // TODO uncomment when findByToken will work
        return /*tagNames*/ Arrays.asList("java", "front-end");
    }

    public Map<String, List<Skill>> getUserSkills(String token) {
        log.warn("getting user skills");
        User user = userService.findByToken(token);

        user.setOwnedSkillIds(Arrays.asList("54e9b7b344ae0e602e9a9f20"));

        List<String> skillIds = user.getOwnedSkillIds();
        List<String> queuedSkillIds = user.getQueuedSkillIds();
        List<Skill> userSkills = Lists.newArrayList(skillRepo.findAll(skillIds));
        //TODO setup queued skills
        List<Skill> queuedSkills = Lists.newArrayList(/*skillRepo.findAll(queuedSkillIds)*/);
        List<Skill> recommendedSkills = getRecommendedUserSkills(new ArrayList<Skill>(){{
            addAll(userSkills);
            addAll(queuedSkills);
        }}, user.getProfession());

        Map<String, List<Skill>> skillsMap = new HashMap<String, List<Skill>>(){{
            put("owned", userSkills);
            put("queued", queuedSkills);
            put("recommended", recommendedSkills);
        }};
        return skillsMap;
    }

    public Skill generateTestSkill() {
        Map<String, Integer> tags = new HashMap<String, Integer>();
        tags.put("testTag", 12);
        return Skill.getBuilder().setName("Test").setTags(tags).build();
    }
}
