package com.headlezz.service;

import com.headlezz.external.model.UserResponse;
import com.headlezz.model.oauth.OauthUser;

public interface AuthorizationService<T extends OauthUser> {
    UserResponse authorize(T oauthUser);
}
