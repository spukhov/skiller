package com.headlezz.service;

import com.headlezz.common.Constants;
import com.headlezz.model.SystemData;
import com.headlezz.model.Tag;
import com.headlezz.repository.SystemDataRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagService {
    @Autowired
    private SystemDataRepo systemDataRepo;
    public List<Tag> getTags() {
        return systemDataRepo.findOne(Constants.SYSTEM_DATA_ID).getTags();
    }

    public void addTag(Tag tag) {
        SystemData data = systemDataRepo.findOne(Constants.SYSTEM_DATA_ID);
        data.getTags().add(tag);
        systemDataRepo.save(data);
    }
}
