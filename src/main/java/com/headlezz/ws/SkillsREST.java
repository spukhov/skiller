package com.headlezz.ws;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.annotation.JacksonFeatures;
import com.headlezz.common.response.RestResponse;
import com.headlezz.external.RestHelper;
import com.headlezz.model.Skill;
import com.headlezz.service.SkillService;
import lombok.extern.log4j.Log4j;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Path("/skills")
@Log4j
public class SkillsREST {
    private static final String LINKEDIN_SKILLS_URL = "https://www.linkedin.com/ta/skill?query=";

    @Autowired
    private SkillService skillService;
    @Autowired
    private RestHelper rest;

    @Path("/{profession}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @JacksonFeatures(serializationEnable = {SerializationFeature.INDENT_OUTPUT})
    public RestResponse getSkillsByProfession(@PathParam("profession") String profession) {
        try {
            List<Skill> result = skillService.getAllSkills("Java Developer");
            return new RestResponse(true, result);
        } catch (Throwable e) {
            return createErrorResponse(e);
        }
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public RestResponse getUserSkills(@HeaderParam("token") String token) {
        log.warn(token != null && !token.isEmpty());
        try {
            Map<String, List<Skill>> skillsMap = null;
            if (token != null && !token.isEmpty()) {
                skillsMap = skillService.getUserSkills(token);
            }
            return  new RestResponse(true, skillsMap);
        } catch (Throwable e) {
            return createErrorResponse(e);
        }
    }




    @Path("/get/{filter}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public RestResponse getSkills(@PathParam("filter") String filter) {
        String jsonResponse = rest.doGet(LINKEDIN_SKILLS_URL + filter);
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            SkillsContainer skillsContainer = mapper.readValue(jsonResponse, SkillsContainer.class);
            List<String> resultList = new ArrayList<String>();
            for (SimpleSkill simpleSkill : skillsContainer.getResultList()) {
                resultList.add(simpleSkill.displayName);
            }
            return new RestResponse(true, resultList);
        } catch (IOException e) {
            e.printStackTrace();

            return createErrorResponse(e);
        }
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public RestResponse saveSkill(Skill skill) {
        try {
            Skill savedSkill = skillService.saveSkill(skill);
            return new RestResponse(true, savedSkill.getId());
        }catch (Exception e){
            return new RestResponse(false, e.getMessage());
        }
    }

    @Path("/add")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public RestResponse addUserSkill(@HeaderParam("token") String token, Skill skill) {
        try {
            skillService.addUserSkill(token, skill.getId());
            return new RestResponse(true, "added skill with id: ");
        }catch (Exception e){
            return new RestResponse(false, e.getMessage());
        }
    }

    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public RestResponse removeUserSkill(@HeaderParam("token") String token, Skill skill) {
        try {
            skillService.addUserSkill(token, skill.getId());
            return new RestResponse(true, "added skill with id: ");
        }catch (Exception e){
            return new RestResponse(false, e.getMessage());
        }
    }


    private RestResponse createErrorResponse(Throwable e) {
        return new RestResponse(false, e.getMessage());
    }

    public static class SkillsContainer {
        private List<SimpleSkill> resultList;

        public List<SimpleSkill> getResultList() {
            return resultList;
        }

        public void setResultList(List<SimpleSkill> resultList) {
            this.resultList = resultList;
        }
    }

    public static class SimpleSkill {
        private String displayName;

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }
    }
}
