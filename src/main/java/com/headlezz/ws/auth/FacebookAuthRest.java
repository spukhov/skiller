package com.headlezz.ws.auth;

import com.headlezz.external.model.Token;
import com.headlezz.external.model.UserResponse;
import com.headlezz.model.oauth.FacebookOauthUser;
import com.headlezz.service.FacebookAuthorizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

@Path("/facebookAuth")
@Component
public class FacebookAuthRest {
    @Autowired
    private FacebookAuthorizationService facebookAuthorizationService;

    @GET
    @Consumes("text/plain")
    @Produces("application/json")
    public UserResponse authGetToken(@Context UriInfo uriInfo) {
        FacebookConnection fbConnection = new FacebookConnection();
        String accessToken = fbConnection.getAccessToken(uriInfo.getQueryParameters().get("code").get(0));
        FacebookUserFetcher facebookUserFetcher = new FacebookUserFetcher(accessToken);
        String graph = facebookUserFetcher.getFBGraph();
        FacebookOauthUser facebookOauthUser = facebookUserFetcher.fetchUser(graph);
        return facebookAuthorizationService.authorize(facebookOauthUser);
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public UserResponse receiveToken(Token facebookToken) {
        FacebookUserFetcher facebookUserFetcher = new FacebookUserFetcher(facebookToken.getCode());
        String graph = facebookUserFetcher.getFBGraph();
        FacebookOauthUser facebookOauthUser = facebookUserFetcher.fetchUser(graph);
        UserResponse userResponse = facebookAuthorizationService.authorize(facebookOauthUser);
        System.out.println(userResponse);
        return userResponse;
    }
}
