package com.headlezz.ws.auth;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//TODO delete this class. It was used to get the URL needed for frontend
public class MainMenu extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private String code = "";

    public void service(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        code = req.getParameter("code");
        if (code == null || code.equals("")) {
            throw new RuntimeException(
                    "ERROR: Didn't get code parameter in callback.");
        }
        FacebookConnection fbConnection = new FacebookConnection();
        String accessToken = fbConnection.getAccessToken(code);

        FacebookUserFetcher fbGraph = new FacebookUserFetcher(accessToken);
        String graph = fbGraph.getFBGraph();
        System.out.println(fbGraph.fetchUser(graph));
    }

    public static void main(String[] args) {
        FacebookConnection fbConnection = new FacebookConnection();
        System.out.println(fbConnection.getFBAuthUrl());
    }
}