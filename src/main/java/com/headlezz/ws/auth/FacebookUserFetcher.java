package com.headlezz.ws.auth;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.headlezz.model.oauth.FacebookOauthUser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class FacebookUserFetcher {
    private String accessToken;
    private ObjectMapper objectMapper = new ObjectMapper();

    public FacebookUserFetcher(String accessToken) {
        this.accessToken = accessToken;
    }

    //TODO rewrite this to HTTP requests
    public String getFBGraph() {
        String graph = null;
        try {

            String urlString = "https://graph.facebook.com/me?" + accessToken;
            URL url = new URL(urlString);
            URLConnection urlConnection = url.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    urlConnection.getInputStream()));
            String inputLine;
            StringBuffer jsonBuffer = new StringBuffer();
            while ((inputLine = in.readLine()) != null)
                jsonBuffer.append(inputLine + "\n");
            in.close();
            graph = jsonBuffer.toString();
            System.out.println(graph);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("ERROR in getting FB graph data. " + e);
        }
        return graph;
    }

    //TODO refactor this shit with exceptions(after redoing to HTTP should be gone)
    public FacebookOauthUser fetchUser(String fbGraph) {
        System.out.println(fbGraph);
        try {
            return objectMapper.readValue(fbGraph, FacebookOauthUser.class);
        } catch (IOException e) {

            return new FacebookOauthUser();
        }
    }
}