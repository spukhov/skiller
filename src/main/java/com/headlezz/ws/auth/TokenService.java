package com.headlezz.ws.auth;

import com.headlezz.model.Credentials;
import com.headlezz.model.User;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Class that generates String token representation
 */
public class TokenService {

    public static final String DATE_PATTERN = "MM dd hh mm:ss";

    public String generateToken(User user) {
        Date currentDate = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_PATTERN);
        String dateString = simpleDateFormat.format(currentDate);
        String token = String.format("%s_%s", user.getEmail(), dateString);
        return token;
    }
}
