package com.headlezz.config;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.authentication.UserCredentials;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@PropertySource("classpath:mongodb.properties")
@EnableMongoRepositories(basePackages = "com.headlezz.repository")
@ComponentScan("com.headlezz")
/*@Import(BatchConfig.class)*/
public class SpringMongoConfig extends AbstractMongoConfiguration {
    @Autowired
    Environment environment;

    @Override
    public String getDatabaseName() {
        return environment.getProperty("db.name");
    }

    @Override
    @Bean
    public Mongo mongo() throws Exception {
        return new MongoClient(environment.getProperty("db.host"),
                Integer.valueOf(environment.getProperty("db.port")));
    }

    @Override
    protected String getMappingBasePackage() {
        return "com.headlezz.repository";
    }

    public
    @Bean
    MongoTemplate mongoTemplate() throws Exception {

        //remove _class
        MappingMongoConverter converter = new MappingMongoConverter(mongoDbFactory(), new MongoMappingContext());
        converter.setTypeMapper(new DefaultMongoTypeMapper(null));

        MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactory(), converter);

        return mongoTemplate;

    }

    @Bean
    public GridFsTemplate gridFsTemplate() throws Exception {
        return new GridFsTemplate(mongoDbFactory(), mappingMongoConverter());
    }

    @Override
    protected UserCredentials getUserCredentials() {
        return new UserCredentials(environment.getProperty("db.user"), environment.getProperty("db.password"));
    }
}
