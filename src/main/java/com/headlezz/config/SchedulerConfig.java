package com.headlezz.config;

import com.headlezz.scheduled.ScheduledTask;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableScheduling
@ComponentScan("com.headlezz.scheduled")
public class SchedulerConfig {
    @Bean
    public ScheduledTask scheduledTask() {
        return new ScheduledTask();
    }
}
