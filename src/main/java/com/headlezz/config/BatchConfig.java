package com.headlezz.config;

import com.headlezz.batch.ListItemReader;
import com.headlezz.batch.FileItemWriter;
import com.headlezz.batch.SkillIndexerTasklet;
import com.headlezz.common.enums.HhAreaCode;
import com.headlezz.external.HeadHunterAPI;
import com.headlezz.external.dto.SearchVacanciesDto;
import org.apache.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.*;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.util.List;

@Configuration
@EnableBatchProcessing
@ComponentScan("com.headlezz")/*
@Import(SpringMongoConfig.class)*/
public class BatchConfig {
    private static final Logger LOG = Logger.getLogger(BatchConfig.class.getName());

    public static final int CHUNK_SIZE = 20;
    private int count;

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private HeadHunterAPI headHunterAPI;

    @Bean
    public Job jobParseHH(Step step1, Step step2) throws Exception {
        return jobBuilderFactory.get("jobParseHH")
                .incrementer(new RunIdIncrementer())
                .start(step1).next(step2)
                .build();
    }

    @Bean
    public Step step1(ItemReader<String> reader, ItemWriter<String> writer) {
        LOG.info("STEP 1");
        return stepBuilderFactory.get("step1")
                .<String, String>chunk(CHUNK_SIZE)
                .reader(reader)
                .writer(writer)
                .build();
    }

    @Bean
    public Step step2(Tasklet tasklet) {
        LOG.info("STEP 2");
        return stepBuilderFactory.get("step2")
                .tasklet(tasklet)
                .build();
    }

    @Bean
    @JobScope
    public ListItemReader reader() throws IOException {
        LOG.info("initializing READER");
        SearchVacanciesDto dto = new SearchVacanciesDto();
        dto.setArea(HhAreaCode.UKRAINE.value());
        dto.setText("java+developer");
        List<String> vacanciesBodies = headHunterAPI.getVacanciesBodies(dto);
        LOG.info("READER list size " + vacanciesBodies.size());

        ListItemReader reader = new ListItemReader(vacanciesBodies);
        return reader;
    }

    @Bean
    @JobScope
    public FileItemWriter writer() {
        FileItemWriter writer = null;
        try {
            writer = new FileItemWriter();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return writer;
    }

    @Bean
    @JobScope
    public SkillIndexerTasklet tasklet() {
        LOG.info("initializing TASKLET");
        return new SkillIndexerTasklet();
    }
}
