package com.headlezz.batch;

import com.headlezz.common.Constants;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemReader;

import java.util.List;

public class ListItemReader implements ItemReader<String> {

    private List<String> list;

    public ListItemReader(List<String> vacanciesBodies) {
        list = vacanciesBodies;
    }

    @Override
    public String read() throws Exception {
        if (!list.isEmpty()) {
            return list.remove(0);
        }
        return null;
    }

    @BeforeStep
    public void initializeValues(StepExecution stepExecution) {
        stepExecution.getJobExecution().getExecutionContext().putInt(Constants.INITIAL_VACANSIES_LIST_SIZE_PROPERTY, list.size());
    }
}
