package com.headlezz.batch;

import com.headlezz.common.Constants;
import com.headlezz.model.Skill;
import com.headlezz.service.SkillService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Component
public class SkillIndexerTasklet implements Tasklet {

    @Autowired
    private SkillService skillService;
    private String fileName;

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        fileName = (String) chunkContext.getStepContext().getJobExecutionContext().get(Constants.FILE_NAME_PROPERTY);
        String fileContent = String.join("\n", Files.readAllLines(Paths.get(fileName)));
        List<Skill> skills = skillService.getAllSkills("java");
        for (Skill skill : skills) {
            int skillMatchCount = 0;
            List<String> keywords = skill.getKeywords();
            for (String keyword : keywords) {
                skillMatchCount += StringUtils.countMatches(fileContent, keyword);
            }
            System.out.println(skill.getName() + " " + skillMatchCount);
        }

        return RepeatStatus.FINISHED;
    }

}
