package com.headlezz.batch;

import com.headlezz.common.Constants;
import org.apache.log4j.Logger;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemWriter;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class FileItemWriter implements ItemWriter<String> {
    private static final Logger LOG = Logger.getLogger(FileItemWriter.class.getName());

    private File file;
    private PrintWriter writer;
    private int count;
    private String fileName;
    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    public FileItemWriter() throws IOException {
        fileName = "data/vacancies" + sdf.format(new Date()) + ".txt";
        file = new File(fileName);
        writer = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
        LOG.info("writing vacancies to: " + fileName);
    }

    @Override
    public void write(List<? extends String> items) throws Exception {
        for (String item : items) {
            writer.println(item.toLowerCase());
        }
        if (count <= items.size()) {
            writer.flush();
            writer.close();
        }

        count -= items.size();
    }

    public void setCount(int count) {
        this.count = count;
    }

    @AfterStep
    public void initializeFile(StepExecution stepExecution) {
        stepExecution.getJobExecution().getExecutionContext().putString(Constants.FILE_NAME_PROPERTY, fileName);
    }

    @BeforeStep
    public void initializeValues(StepExecution stepExecution) {
        count = stepExecution.getJobExecution().getExecutionContext().getInt(Constants.INITIAL_VACANSIES_LIST_SIZE_PROPERTY);
    }
}
