package com.headlezz.external;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.headlezz.external.dto.SearchVacanciesDto;
import com.headlezz.external.model.Vacancy;
import com.headlezz.external.model.VacancyShort;
import com.headlezz.external.model.VacancyShortWrapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Component
@Scope(BeanDefinition.SCOPE_SINGLETON)
public class HeadHunterAPI {
    public static final String BASE_URL = "https://api.hh.ru";
    public static final String VACANCIES = "/vacancies";
    public static final String TEXT = "text=%s";
    public static final String AREA = "area=%s";
    public static final String SPECIALIZATION = "specialization=1.221";
    public static final String PER_PAGE = "per_page=50";
    public static final String PAGE = "page=%s";
    private static final Logger LOG = Logger.getLogger(HeadHunterAPI.class.getName());

    @Autowired
    private RestHelper rest;


    public static String getBaseUrl() {
        return BASE_URL;
    }

    public List<String> getVacanciesBodies(SearchVacanciesDto searchVacanciesDto) throws IOException {
        LOG.info("---GETTING VACANCIES");
        long start = System.currentTimeMillis();

        List<VacancyShort> vacanciesShortList = getVacancies(searchVacanciesDto);
        List<String> bodies = new ArrayList<String>();
        for (VacancyShort vc : vacanciesShortList) {
            bodies.add(getVacancyById(vc.getId()).getDescription());
        }

        long end = System.currentTimeMillis();
        System.out.println("HH API took ms: " + (end - start));
        return bodies;
    }

    private Vacancy getVacancyById(String id) throws IOException {
        String vacancy = rest.doGet(BASE_URL + VACANCIES + "/" + id);
        ObjectMapper mapper = new ObjectMapper();
        return vacancy != null ? mapper.readValue(vacancy, Vacancy.class) : new Vacancy() {
        };
    }

    public List<VacancyShort> getVacancies(SearchVacanciesDto searchVacanciesDto) throws IOException {
        System.out.println("---Working with HH API!");
        VacancyShortWrapper vw = getVacancyShortWrapper(searchVacanciesDto, VacancyShortWrapper.class);
        List<VacancyShort> vacanciesShort = new ArrayList<VacancyShort>(Arrays.asList(vw.getItems()));
        int pages = vw.getPages();
        for (int i = 1; i <= pages + 1; i++) {
            searchVacanciesDto.setPage(i);
            List<VacancyShort> vacancyShorts = Arrays.asList(getVacancyShortWrapper(searchVacanciesDto, VacancyShortWrapper.class)
                    .getItems());
            vacanciesShort.addAll(vacancyShorts);
        }
        return vacanciesShort;
    }

    private VacancyShortWrapper getVacancyShortWrapper(SearchVacanciesDto searchVacanciesDto, Class clazz) throws IOException {
        String url = prepareVacanciesSearchUrl(searchVacanciesDto);
        LOG.info("GET " + url);
        String response = rest.doGet(url);
        ObjectMapper mapper = new ObjectMapper();
        return (VacancyShortWrapper) mapper.readValue(response, clazz);
    }

    private String prepareVacanciesSearchUrl(SearchVacanciesDto searchVacanciesDto) {
        String text = searchVacanciesDto.getText();
        String area = searchVacanciesDto.getArea();
        int page = searchVacanciesDto.getPage();
        return BASE_URL + VACANCIES + "?" + /*SPECIALIZATION + "&" + */String.format(TEXT, text) +
                "&" + String.format(AREA, area) + "&" + PER_PAGE + "&" + String.format(PAGE, page);
    }

}
