package com.headlezz.external.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VacancyShortWrapper {
    private VacancyShort[] items;
    private int pages;

    public VacancyShort[] getItems() {
        return items;
    }

    public void setItems(VacancyShort[] items) {
        this.items = items;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }
}
