package com.headlezz.external.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
@JsonIgnoreProperties(ignoreUnknown = true)

public class CoursesShort {
@JsonProperty("elements")
    private List<CourseShort> elements;

    public List<CourseShort> getElements() {
        return elements;
    }

    public void setElements(List<CourseShort> elements) {
        this.elements = elements;
    }
}
