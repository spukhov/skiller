package com.headlezz.external;

import org.springframework.stereotype.Component;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
public class RestHelper {

    private Client client = ClientBuilder.newClient();

    public String doGet(String url) {
        String s = "";
        try {
            s = client.target(url).
                    request(MediaType.APPLICATION_JSON).get(String.class);
        } catch (Exception e) {
            System.out.println("Exception while GET " + url + ": " + e.getMessage());
            return null;
        }
        return s;
    }

    public Response doPost(String url, Entity entity) {
        return client.target(url).
                request(MediaType.APPLICATION_JSON).post(entity);
    }


}
