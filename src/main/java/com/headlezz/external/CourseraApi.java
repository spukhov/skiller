package com.headlezz.external;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.headlezz.external.model.CourseShort;
import com.headlezz.external.model.CoursesShort;
import com.headlezz.external.util.CoursesShortQueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
@Scope(BeanDefinition.SCOPE_SINGLETON)
public class CourseraApi {
    @Autowired
    private RestHelper rest;

public CoursesShort getCourses(List<String> keywords) throws IOException {
    CoursesShortQueryBuilder builder = new CoursesShortQueryBuilder();
    builder.addParams(keywords);
    String coursesResult = rest.doGet(builder.toString());
    ObjectMapper mapper = new ObjectMapper();
  //  List<CourseShort> courses = mapper.readValue(TypeFactory.collectionType(List.class, CourseShort.class));
    CoursesShort coursesShort;
    if (coursesResult==null) {
        coursesShort = new CoursesShort();
        coursesShort.setElements(new ArrayList<CourseShort>());
    }
    else {
         coursesShort = mapper.readValue(coursesResult, CoursesShort.class);
    }
    return coursesShort;
}


}
