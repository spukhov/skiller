package com.headlezz.external.util;


import com.headlezz.model.User;
import com.headlezz.model.oauth.FacebookOauthUser;

public class FacebookUserConverter implements UserConverter<FacebookOauthUser> {
    @Override
    public FacebookOauthUser convertFromSkillerUser(User skillerUser) {
        FacebookOauthUser facebookOauthUser = new FacebookOauthUser();
        facebookOauthUser.setEmail(skillerUser.getCreds().getEmail());
        return facebookOauthUser;
    }

    @Override
    public User convertToSkillerUser(FacebookOauthUser facebookOauthUser) {
        User user = new User();
        user.setEmail(facebookOauthUser.getEmail());
        user.setName(facebookOauthUser.getName());
        return user;
    }
}
