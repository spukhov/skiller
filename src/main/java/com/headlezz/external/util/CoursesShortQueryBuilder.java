package com.headlezz.external.util;


import java.util.List;

public class CoursesShortQueryBuilder {
    private static final String URL = "https://api.coursera.org/api/catalog.v1/courses?q=search&query=";
    private StringBuilder builder;

    public CoursesShortQueryBuilder() {
        builder = new StringBuilder(URL);
    }

    public CoursesShortQueryBuilder addParam(String value) {
        builder.append("+").append(value);
        return this;
    }

    public CoursesShortQueryBuilder addParams(List<String> values) {
        for (int i = 0; i<values.size(); i++) {
            if (i<values.size()-1) {
                builder.append("+");
            }
            builder.append(values.get(i));
        }
        return this;
    }

    @Override
    public String toString() {
        return builder.toString();

    }
}
