package com.headlezz.external.util;

import com.headlezz.model.User;
import com.headlezz.model.oauth.OauthUser;

public interface UserConverter<T extends OauthUser> {
    T convertFromSkillerUser(User skillerUser);

    User convertToSkillerUser(T oauthUser);


}
