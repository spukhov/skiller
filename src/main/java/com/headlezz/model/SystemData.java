package com.headlezz.model;

import com.headlezz.common.Constants;
import org.springframework.data.annotation.Id;

import java.util.List;

public class SystemData {
    @Id
    private String id = Constants.SYSTEM_DATA_ID;
    private List<Tag> tags;

    public String getId() {
        return id;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }
}
