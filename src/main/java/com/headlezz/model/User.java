package com.headlezz.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;

import java.util.List;

@Getter
@Setter
@ToString
public class User {
    @Id
    private String id;
    private Credentials creds;
    private String profession;
    private List<String> ownedSkillIds;
    private List<String> queuedSkillIds;
    private List<String> tagNames;
    private String email;
    private String name;
}
