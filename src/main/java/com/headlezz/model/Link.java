package com.headlezz.model;

/**
 * Created by FAT on 21.02.2015.
 */
public class Link {
    private String name;
    private String type;
    private String url;

    public Link(String name, String type, String url) {
        this.name = name;
        this.type = type;
        this.url = url;
    }
    public Link(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
