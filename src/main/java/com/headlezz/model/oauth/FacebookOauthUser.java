package com.headlezz.model.oauth;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@ToString(callSuper=true)
public class FacebookOauthUser extends OauthUser {
    @JsonProperty("id")
    private String id;
    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("gender")
    private String gender;
    @JsonProperty("locale")
    private String locale;
    @JsonProperty("last_name")
    private String lastName;
    @JsonProperty("timezone")
    private Integer timeZone;
    @JsonProperty("updated_time")
    private Date updatedTime;
    @JsonProperty("verified")
    private Boolean verified;
    @JsonProperty("link")
    private String link;
    @JsonProperty("name")
    private String name;
}
