package com.headlezz.model;

import com.headlezz.common.enums.SourceType;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

import java.util.List;

@Getter
@Setter
public class Source {
    @Id
    private String id;
    private SourceType type;
    private String name;
    private String description;
    private String url;
    private int vote;
    private List<String> votedIds;

}
