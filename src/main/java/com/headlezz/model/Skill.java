package com.headlezz.model;


import com.headlezz.common.enums.SkillType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;

import java.util.List;
import java.util.Map;

@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"name", "description", "type"})
public class Skill {
    @Id
    private String id;
    private String name;
    private String description;
    private SkillType type;
    private String imageUrl;
    private List<String> keywords;
    private Map<String, Integer> tags;
    private List<String> sourceIds;
    private List<TagRank> tagRanks;

    public Skill() {    }

    public static class SkillBuilder {
        private Skill skill;
        private SkillBuilder(){
            skill = new Skill();
        }

        public SkillBuilder setName(String name) {
            skill.setName(name);
            return this;
        }

        public SkillBuilder setDescription(String description) {
            skill.setDescription(description);
            return this;
        }

        public SkillBuilder setType(SkillType type) {
            skill.setType(type);
            return this;
        }

        public SkillBuilder setImageUrl(String imageUrl) {
            skill.setImageUrl(imageUrl);
            return this;
        }

        public SkillBuilder setKeywords(List<String> keywords) {
            skill.setKeywords(keywords);
            return this;
        }
        public SkillBuilder setTags(Map<String, Integer> tags) {
            skill.setTags(tags);
            return this;
        }
        public SkillBuilder setSourcesIds(List<String> sourcesIds) {
            skill.setSourceIds(sourcesIds);
            return this;
        }
        public SkillBuilder setRates(List<TagRank> tagRanks) {
            skill.setTagRanks(tagRanks);
            return this;
        }

        public Skill build(){
            return skill;
        }
    }

    public static SkillBuilder getBuilder() {
        return new SkillBuilder();
    }

}
