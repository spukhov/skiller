package com.headlezz.model;

import org.springframework.data.annotation.Id;

public class Tag {
    @Id
    private String name;
    private Object description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }
}
