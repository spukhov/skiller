package com.headlezz.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class TagRank {
    private String tagName;
    private float rank;
}
