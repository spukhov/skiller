package com.headlezz.common.response;

public class RestResponse {
    private boolean result;
    private Object body;

    public RestResponse(boolean result, Object body) {
        this.result = result;
        this.body = body;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }
}
