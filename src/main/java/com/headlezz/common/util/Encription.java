package com.headlezz.common.util;


import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;import java.lang.String;

public class Encription {
    public static BCryptPasswordEncoder bCrypt = new BCryptPasswordEncoder();

    public static String encode(String s) {
        return bCrypt.encode(s);
    }

    public static boolean checkPassword(String first, String second) {
        return bCrypt.matches(first, second);
    }
}
