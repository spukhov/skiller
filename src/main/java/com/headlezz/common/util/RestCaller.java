package com.headlezz.common.util;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Map;

public class RestCaller<T> {

    public Response get(String url) {
        return ClientBuilder.newClient().target(url).
                request(MediaType.APPLICATION_JSON).get();
    }

    public Response post(String url, T t) {
        return ClientBuilder.newClient().target(url).
                request(MediaType.APPLICATION_JSON).post(Entity.entity(t, MediaType.APPLICATION_JSON));
    }

    public Response get(String url, Map<String, String> headerParams) {
        return null;
    }

}
