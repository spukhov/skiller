package com.headlezz.common.enums;

public enum SourceType {
    COURSE, BOOK, ARTICLE, QUIZ, OTHER
}
