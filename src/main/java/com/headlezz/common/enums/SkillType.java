package com.headlezz.common.enums;

public enum SkillType {
    LANGUAGE("LANGUAGE"), FRAMEWORK("LANGUAGE"), TOOL("TOOL");

    private String name;

    SkillType(String n) {
        name = n;
    }

    public String value() {
        return name;
    }

    public SkillType forValue(String v) {
        for (SkillType st : values()) {
            if (st.value().equalsIgnoreCase(v)) {
                return st;
            }
        }
        return null;
    }
}
