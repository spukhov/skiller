package com.headlezz.common.enums;

public enum HhAreaCode {
    UKRAINE("5"), KYIV("115");

    String code;

    HhAreaCode(String code) {
        this.code = code;
    }

    public String value() {
        return code;
    }

    public HhAreaCode forValue(String v) {
        for (HhAreaCode code : values()) {
            if (code.value().equalsIgnoreCase(v)) {
                return code;
            }
        }
        return null;
    }
}
