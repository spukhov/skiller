package com.headlezz.common;

public class Constants {
    public static final String IMAGE_STORAGE_WEB_PATH = "img/";
    public static final String MY_LOCAL_WEBAPP = "/home/pooh/workspace/java/nonamer/src/main/webapp/";
    public static final String FILE_NAME_PROPERTY = "FILE_NAME";
    public static final String INITIAL_VACANSIES_LIST_SIZE_PROPERTY = "VACANCIES_SIZE";

    public static final String OCCUPATION_JAVA = "Java Developer";

    public static final String SYSTEM_DATA_ID = "SKILLER";

}
