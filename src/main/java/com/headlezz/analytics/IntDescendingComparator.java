package com.headlezz.analytics;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class IntDescendingComparator extends WritableComparator {
    public IntDescendingComparator() {
        super(IntWritable.class, true);
    }

    @Override
    public int compare(WritableComparable a, WritableComparable b) {
        IntWritable int1 = ((IntWritable) a);
        IntWritable int2 = ((IntWritable) b);
        if (int1.compareTo(int2)==1) return -1;
        if (int1.compareTo(int2)==-1) return 1;
        return 0;

    }
}
