package com.headlezz.analytics;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class HhAnalyzer {

    public static final String INPUT_DIR_WC = "/home/pooh/workspace/in";
    public static final String OUTPUT_DIR_WC = "/home/pooh/workspace/out";
    public static final String INPUT_DIR_SORT = "/home/pooh/workspace/inSort";
    public static final String OUTPUT_DIR_SORT = "/home/pooh/workspace/outSort";

    public void run() throws IOException, ClassNotFoundException, InterruptedException {
        Runtime.getRuntime().exec("cp /home/pooh/workspace/java/skiller/test.txt " + INPUT_DIR_WC);
        executeWordCountJob();
        executeSortJob();
        Runtime.getRuntime().exec("cp " + OUTPUT_DIR_SORT + "/part-r-00000 /home/pooh/workspace/java/skiller/hh_result.txt");
    }

    private void executeWordCountJob() throws IOException, InterruptedException, ClassNotFoundException {
        Runtime.getRuntime().exec("rm -rf " + OUTPUT_DIR_WC);
        Configuration configuration = new Configuration();
        Job job = Job.getInstance(configuration, "skillCount");

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        job.setMapperClass(HhWordMapper.class);
        job.setReducerClass(HhWordReducer.class);

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        FileInputFormat.addInputPath(job, new Path(INPUT_DIR_WC));
        FileOutputFormat.setOutputPath(job, new Path(OUTPUT_DIR_WC));
        job.waitForCompletion(true);
    }

    private void executeSortJob() throws IOException, InterruptedException, ClassNotFoundException {
        Runtime.getRuntime().exec("rm -rf " + OUTPUT_DIR_SORT);
        Runtime.getRuntime().exec("cp " + OUTPUT_DIR_WC + "/part-r-00000 " + INPUT_DIR_SORT + "/in.txt");
        Configuration configuration = new Configuration();
        Job job = Job.getInstance(configuration, "skillCountSort");

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        job.setMapperClass(HhSortMapper.class);
        job.setReducerClass(HhSortReducer.class);

        job.setSortComparatorClass(IntDescendingComparator.class);

        job.setMapOutputKeyClass(IntWritable.class);
        job.setMapOutputValueClass(Text.class);

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        FileInputFormat.addInputPath(job, new Path(OUTPUT_DIR_WC));
        FileOutputFormat.setOutputPath(job, new Path(OUTPUT_DIR_SORT));
        job.waitForCompletion(true);
    }
}
