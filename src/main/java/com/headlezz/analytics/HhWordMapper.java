package com.headlezz.analytics;


import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

public class HhWordMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
    private final static IntWritable one = new IntWritable(1);
    private Text word = new Text();
    public static final String DELIMITERS = " \n\r\t\f,.:;'()<>/+-“\"";
    private List<String> htmlTags = Arrays.asList("a", "p", "div", "ul", "li", "ol", "strong", "i", "bold", "br");
    private List<String> parasites = Arrays.asList("ciklum", "and", "of", "in", "the", "to", "with", "experience", "•", "for", "development", "is", "team", "on", "knowledge",
            "as", "our", "skills", "spftware", "project", "work", "we", "it", "business", "or", "you", "test", "technical", "good", "their",
            "management", "agile", "are", "working", "web", "requirements", "company", "will", "years", "client", "an", "new", "that",
            "technoligies", "environment", "technologies", "your", "have", "ability", "application", "applications", "responsibilities", "teams", "from", "products", "product", "other", "understanding", "more", "using", "support", "based", "plus",
            "over", "required", "join", "system", "innovative", "solutions", "companies", "em", "projects", "be", "&amp", "looking", "including", "software", "systems", "clients", "they", "infrastructure", "top", "cool", "specialists", "centers", "outsourcing","leading", "activities", "consulting", "administrative");

    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();
        StringTokenizer tokenizer = new StringTokenizer(line, DELIMITERS);
        while (tokenizer.hasMoreElements()) {
            String token = tokenizer.nextToken().toLowerCase();
            if (isTokenValid(token)) {
                word.set(token);
                context.write(word, one);
            }
        }
    }

    private boolean isTokenValid(String token) {
        if (!htmlTags.contains(token) && !parasites.contains(token) && !isCyrillicByFirstChar(token)) {
            return true;
        }
        return false;
    }

    private boolean isCyrillicByFirstChar(String s) {
        if (Character.UnicodeBlock.CYRILLIC.equals(Character.UnicodeBlock.of(s.toCharArray()[0]))) {
            return true;
        }
        return false;
    }

}
