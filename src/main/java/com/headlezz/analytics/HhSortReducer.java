package com.headlezz.analytics;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class HhSortReducer extends Reducer<IntWritable, Text, Text, IntWritable> {
    private Text val = new Text();

    @Override
    protected void reduce(IntWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        while (values.iterator().hasNext()) {
            val = values.iterator().next();
            context.write(val, key);
        }
    }

}
