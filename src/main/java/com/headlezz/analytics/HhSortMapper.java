package com.headlezz.analytics;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class HhSortMapper extends Mapper<LongWritable, Text, IntWritable, Text> {
    private Text name = new Text();
    private IntWritable count = new IntWritable();

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();

        String[] splitted = line.split("\t");

        if (splitted.length == 2) {
            int c = Integer.valueOf(splitted[1]);
            if (c < 5) {
                return;
            }
            name.set(splitted[0]);
            count.set(c);
            context.write(count, name);
        }

    }
}
