package com.headlezz.repository;

import com.headlezz.model.SystemData;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface SystemDataRepo extends MongoRepository<SystemData, String> {
}
