package com.headlezz.email.util;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;
import org.springframework.stereotype.Service;

@Service
public class EmailService {

    private static final String SENDER_EMAIL = "";
    private static final String PASSWORD = "";
    private static final String EMAIL_SUBJECT = "";

    public void sendEmailTo(String receiversEmail) throws MessagingException {
        Message message = createMessage(receiversEmail, EMAIL_SUBJECT, generateEmailBody());
        Transport.send(message);
    }

    private Message createMessage(String receiversEmail, String subject, String text) throws MessagingException {
        Session session = createSession(SENDER_EMAIL, PASSWORD);
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(SENDER_EMAIL));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(receiversEmail));
        message.setSubject(subject);
        message.setText(text);
        return message;
    }

    private Session createSession(final String userName, final String pass) {
        Session session = Session.getInstance(createProperties(),
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(userName, pass);
                    }
                });
        return session;
    }

    private Properties createProperties() {
        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        return props;
    }

    private String generateEmailBody() {
        return "email body";//TODO message body generator
    }
}